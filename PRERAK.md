# Topic - Efficient Deep Learning - Model compression via pruning for object detection

# Work Done
| Week |      Date       |      Topic            |                        Work Done                       |
| ---- | --------------- | ---------------       | -----------------------------------------------------  |  
|   1  |  21/04 - 27/04  |   Research            | Read up on all 3 model-compression types for comparison|
|   2  |  28/04 - 04/05  |   Object Detection    | Read up on SSD  |   
|   3  |  05/05 - 11/05  |   Code Play - Obj Det |   |
|   4  |  12/05 - 18/05  |   Code Play - Comp    |   |

# References
1. [PASCAL VOC 2007 Dataset](http://host.robots.ox.ac.uk/pascal/VOC/voc2007/index.html)
2. [YOLO Repo](https://github.com/xiongzihua/pytorch-YOLO-v1/blob/master/dataset.py)